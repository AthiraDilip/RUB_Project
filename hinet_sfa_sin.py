import mdp
import numpy as np
import matplotlib.pyplot as plt


p = np.pi * 2
t = np.linspace(0, 1, 10000, endpoint = 0)
signal1 = np.sin(p * 5 *t)
signal2 = np.sin(p * 11 *t)
signal3 = np.sin(p * 13 *t) 
complex_sin = np.sin(p * 5 *t) + np.sin(p * 11 *t) + np.sin(p * 13 *t) 

noise = np.random.normal(0,0.2,10000)

noisenode = mdp.nodes.NormalNoiseNode(input_dim= 10000 *1 , noise_args=(0, 0.01))
complex_sin_noise = complex_sin + noise
plt.figure(0)
'''
plt.plot(t, complex_sin, 'k')
plt.plot(t, complex_sin_noise, 'y')
plt.plot(t, signal1, 'g')
plt.plot(t, signal2, 'c')
plt.plot(t, signal3, 'm')
'''

series = np.zeros((10000, 5), 'd')
series[:,3] = np.transpose(complex_sin)
series[:,4] = np.transpose(complex_sin_noise)
series[:,0] = np.transpose(signal1)
series[:,1] = np.transpose(signal2)
series[:,2] = np.transpose(signal3)

sum_signals = np.sum(series, axis =1)
print np.shape(sum_signals)
plt.plot(t, sum_signals, 'm')


sfa =  mdp.nodes.SFANode(input_dim =2, output_dim=2) 
sfa2 =  mdp.nodes.SFANode(input_dim =2,output_dim=1) 

'''
Without use of PCANode for output dim of sfa > 2 mdp.NodeException
mdp.NodeException: Got negative eigenvalues: [ -3.66210938e-05   9.86860836e-06   4.77639121e-05]. 
You may either set output_dim to be smaller, or prepend the SFANode with a PCANode(reduce=True) or PCANode(svd=True)
'''
pca = mdp.nodes.PCANode(input_dim=5,output_dim=4) 
sfa_layer = mdp.hinet.Layer([sfa, sfa2])

switchboard = mdp.hinet.Switchboard(input_dim = 4, connections = [1,0,2,1])
flow = pca + switchboard + sfa_layer

slow = flow.execute(series)
plt.plot(t, slow[:,0], 'r')
#plt.plot(t, slow[:,1], 'm')
#plt.plot(t, slow[:,2], 'g')


sfa3 = mdp.nodes.SFANode(input_dim=4,output_dim=2)
flow_sfa = pca + sfa3
slow_sfa = flow_sfa.execute(series)
plt.plot(t, slow_sfa[:,0], 'b')
plt.show()