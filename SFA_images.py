import mdp
import numpy as np
import matplotlib.pyplot as plt
import cv2
import os


sfa =  mdp.nodes.SFA2Node(output_dim=10) 
'''
Without use of PCANode for output dim of sfa > 2 mdp.NodeException
mdp.NodeException: Got negative eigenvalues: [ -3.66210938e-05   9.86860836e-06   4.77639121e-05]. 
You may either set output_dim to be smaller, or prepend the SFANode with a PCANode(reduce=True) or PCANode(svd=True)
'''
pca = mdp.nodes.PCANode() 
flow = pca + sfa 
#flownode = mdp.hinet.FlowNode(mdp.Flow(flow))

count = 0
folderName = '/home/guest-cns/Project_Misc/images/'
for path, dirs, files in os.walk(folderName):
  for fileName in files:
    filePath = os.path.join(folderName, fileName)
    img = cv2.imread(filePath)
    im = np.float32(img)
    x_data = np.float32(img)
    
    flat_x = x_data.flatten()
    reIm =flat_x.reshape((1,560*320*3))
    if count == 0:
      reIm_array = reIm
      '''
    elif count < 20:
      break
    elif count >50:
      break
      '''
    else :
      reIm_array = np.append(reIm_array,reIm,axis = 0)
    count = count +1 

print np.shape(reIm_array)

dir_results = '/home/guest-cns/Codebase_Learning/current_results/'
slow = flow.execute(np.transpose(reIm_array))
count = 0
while count < sfa.output_dim:
  slowest = slow[:,count]
  slowest = slowest.reshape(320,560,3)
  print(np.shape(slow))
  slowest = cv2.normalize(slowest, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
  cv2.imwrite( str(dir_results) + "result%d.jpg" % count, slowest)
  count = count + 1
