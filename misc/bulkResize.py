import os
import sys
import PIL
from PIL import Image

def resize(folder, fileName):
    filePath = os.path.join(folder, fileName)
    im = Image.open(filePath)
    print fileName
    newIm = im.resize((50, 40), PIL.Image.ANTIALIAS)
    newIm.save('/home/guest-cns/Project_Misc/images1resized/' + fileName[:-4] + 'resized.jpg')

def bulkResize(imageFolder):
   
    for path, dirs, files in os.walk(imageFolder):
        for fileName in files:
            ext = fileName[-3:].lower()
            resize(path, fileName)

if __name__ == "__main__":
    imageFolder = '/home/guest-cns/Project_Misc/images/'
    bulkResize(imageFolder)
    
